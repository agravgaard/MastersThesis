name = Thesis

all: $(name).pdf $(name).tgz

$(name).pdf: tex/$(name).tex tex/*.tex MyLibrary.bib
	pdflatex -interaction=nonstopmode -shell-escape $<
	bibtex $(name)
	pdflatex -interaction=nonstopmode -shell-escape $<
	pdflatex -shell-escape $<


clean:
	$(RM) $(name).pdf *.o *.log *.aux *.bbl *.log *.toc *.out tex/*.aux *.blg

$(name).tgz : Makefile tex/*.tex $(name).pdf
	tar --file $@ --create --gzip $^
	@echo "have just archived the following files"
	@tar --file $@ --list

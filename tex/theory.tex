\chapter{Theory}

\section{Computed Tomography scans}

A \ac{CT} is a 3D representation of the electron density of an object, that
object is usually a human in the case of cancer therapy. It is created by
measuring the attenuation of X-rays through said object. With a single X-ray
source on one side of the object and an array of sensors on the other. In the
case of \ac{CT} images the array is thin and rotates fast around the object.
This has the benefit that almost only directly transmitted photons will be
absorbed in the sensor-array and only few scattered photons. However, in the
case of \ac{CB} \ac{CT}, the array is wide and only one or half a rotation is
usually performed, as the scanner is mounted on the treatment gantry, which can
only do approximately one rotation per minute. This has the main benefit that
you get the anatomy of the patient at the time of treatment, and with the
isocenter aligned by definition with that of the gantry rotation and thereby
beam geometry. The wide \ac{CB} sensor-array gives a better superior-inferior
($z$-direction in cylindrical coordinate system of gantry rotation) resolution
than a \ac{CT} scanner, as the \ac{CT} scanner virtually records one
\textit{slice} per rotation and then moves $\approx 3$ mm, to record next
slice. However, the wide sensor-array also has the downside that it will
capture a lot of the scattered photons, thus the reconstruction of \ac{CB}
projections with scatter into a \ac{CBCT} will result in \textit{artifacts} in
the \ac{CBCT}.

For regular radiotherapy \ac{CBCT} images give a sufficiently correct image of
how the photons from the treatment will scatter. However, for protons, even the
correctness of the \ac{CT} is questionable when calculating stopping power. For
the first study it is assumed that the electron density map of the \ac{CT} is
directly transferable to the stopping power of protons \cite{Schaffner1998}.
This assumption is corrected for by using the libdEdx made available by
\cite{Luhr2012} as implemented with PyTRiP \cite{Toftegaard2014}. In the second
study it  is assumed that the \ac{CT} is correct and compare differently
reconstructed \ac{CBCT}s to this.  \medskip

The \ac{CT} and the \ac{CBCT} consists of a cube of values each with a physical
position given by the location in the 3D-matrix times the pixel size in x and y
directions and the slice distance in the z direction plus an offset, these are
called voxel - volume pixels. The value of the voxel is in Hounsfield units
which is given by \eqref{eq:HU}:

\begin{align}
	HU = 1000 \times \frac{\mu-\mu_{water}}{\mu_{water}-\mu_{air}}
	\label{eq:HU}
\end{align}

Where $\mu$ is the linear attenuation coefficient, given by
\textit{backprojection} of the measured attenuation into a voxel-cube. While
$\mu_{water}$ and $\mu_{air}$ is the linear attenuation coefficient for water
and air respectively. 

\section{Volumes of Interest} 

To receive the location of the organs in the \ac{CT} images, a physician
delineates all \ac{VOI}. Each \ac{VOI} is given a category corresponding to how
it should be treated. The \ac{GTV} outlines the tumour itself, in some of the
treatments the \ac{GTV} is removed by surgery and RT is given to the remaining
cells with a risk of being cancerous. \medskip

The \ac{CTV} outlines the cells with a risk of being cancerous while the
\ac{ITV}) is supposed to take account for movement, such as respiratory, setup
and inter-fractional changes. The \ac{PTV} is typically outlined by adding a
margin to the \ac{ITV} or \ac{CTV} to make sure the whole \ac{CTV} receives the
prescribed dose. The \ac{PTV} is outlined by the physicist, during the dose
planning with the corresponding margin configuration (\fref{fig:TVs}).
\cite{Podgorsak2006} \medskip

\ac{OR} are also delineated, and as the name suggests, are the organs which
either are fragile, will lose function or, most importantly in the case of
paediatrics, develop secondary cancer when receiving too high a dose.  However,
the main limiting dose factor are the side effects, e.g. for the pelvic region
may include diarrhea, rectal bleeding, incontinence, bladder irritation, and
sexual dysfunction. These organs are therefore delineated to know the delivered
dose. \medskip

\begin{figure}
  \centering
  \includegraphics[width=.7\linewidth]{img/TVs.png}
  \caption{Graphical representation of the volumes of interest, as 
  defined in ICRU Reports No. 50 and 62.\cite{Podgorsak2006}}\label{fig:TVs}
\end{figure}

\section{Margins} 

When the \ac{VOI}s in the images are delineated a contour is created, which is
a list of points corresponding to the points on each slice enclosing the
volume. To use these contours for dose calculations the points are translated,
using the size of the \ac{CT} cube, the pixel size and the slice distance, to
make a boolean cube with the same dimensions as the \ac{CT} cube with ones
in the \ac{VOI} and zeroes elsewhere. \medskip

To make the \ac{CTV}-to-\ac{PTV} margin for the specific plan the custom is to
use uncertainties in the beam direction and laterally from this. However, to
investigate the dose degradation dependence on beam direction, isotropic
margins was used, expanding the volume as a boolean cube, by step-wise adding
ones in all points within a given margin e.g. $7 \mathrm{mm}$ around the
original volume.

\section{Treatment planning}

A \ac{TPS}\cite{Podgorsak2006} for particle therapy adds intensity weighted
Bragg-peaks and correcting for the changes in stopping power from the \ac{CT}
until the target is covered\cite{Podgorsak2006}. A typical proton beam is
approximately symmetric and Gaussian shaped with a \ac{FWHM} of $5
\mathrm{mm}$, yielding a lateral dose fall-off. To cover the target these beams
are positioned in a spacing of $2 \mathrm{mm}$ and $2 \mathrm{mm}$ lateral
Bragg peak size, which is approximately the same as the \ac{CT} slice
resolution ranging between $2 ~\text{to} ~3 \mathrm{mm}$.  TRiP98 then
calculates the dose from these beams by \eqref{eq:KramerDose}
\cite{Kramer2000}.

\begin{align}
  D(E_{beam},x) = (1.6 \times 10^{-8}) d(E_{beam},z) \frac{N}{2 \pi \sigma^2} \exp(-\frac{1}{2}\frac{r^2}{\sigma^2})
  \label{eq:KramerDose}
\end{align}

\eqref{eq:KramerDose} Describes the dose generated by a single ion beam, with
energy $E_{beam}$ centred at $(x_0,y_0)$. This model does not take
\textit{multiple scattering} and particles($\gamma$ and $\delta$-rays)
generated by the slowing down of the ions into account. $r$ is the distance
from the beam centre, $\sigma$ the width of the Gaussian beam profile, while
$N$ is the total number of particles.  $d(E_{beam},z)$\eqref{eq:Kramerd} is the
energy loss distribution for the given initial beam energy as a function of
depth, $z$.

\begin{align}
	d(E_{beam},z) = \sum_T{\int_E dE \frac{dN}{dE}(E_{beam},z,T,E) \frac{dE}{\rho dx}(T,E)}
	\label{eq:Kramerd}
\end{align}

With the contents of the integral being the fluence of the beam, times
the mass stopping power, with T describing species.

\section{Dose Volume Histograms and Prescribed dose}

When dose plans are made clinically there are dose constraints which must be
met for the plan to be approved, these are decided nationally for every cancer
type. A typical constraint tells that a certain percentage of the volume of a
specific OR at max can receive a certain amount of dose, either in Gy or in
percentage of the prescribed dose to the tumour target, which have a similar
constraint. \medskip

To meet these constraints \ac{DVH} are used, which are the Dose as percent
($D_{\%}$) or in Gy against the Volume percent ($V_{\%}$) receiving this dose.
These curves are most often showed as cumulative plots \cite{Podgorsak2006}.
\ac{DVH} curves are calculated using the delineations of the volumes and the
dose matrix generated by the \ac{TPS}.

\section{CBCT}

The \ac{CBCT} reconstruction back-projects the \ac{CB} projections into an
empty 3D cube roughly by tracing along the path from every pixel in the
projection and to the X-ray source position for that projection, a simple
illustration of this can be seen in \fref{fig:BP}.  Before doing the actual
back-projection, the projections are usually filtered in the Fourier space,
with a Ram-Lak filter multiplied by either a Hann, Hamming, Cosine, or a
similar function, or a combination of these. The filtered back projection
method is often referred to as a Feldkamp Davis Kress (\ac{FDK})
reconstruction\cite{Feldkamp1984}, which is the algorithm that is implemented
in most reconstruction software, e.g. the Reconstruction Toolkit
(\ac{RTK})\cite{Rit2014} and Plastimatch\cite{Sharp2007}, which have
implementations in C++, \ac{CUDA} and \ac{OpenCL}. Because medical images
usually are in sizes divisible by 64 and that the back-projection can be done
in parallel for each voxel, the Graphics Processing Unit (\ac{GPU})
implementations (in \ac{CUDA} and \ac{OpenCL}) can give a manifold speedup to
the computation. 

In this project the inverse process was also used, i.e. forward-projection or a
\ac{DRR}, to simulate the \ac{CB} projections from a \ac{CT} image. This
calculation is very similar to the WEPL calculation, but far more compute
intensive than the back-projection. However, it is even more parallelizable
than the back-projection and the \ac{GPU} implementations in \ac{RTK} and
Plastimatch are therefore quite fast.

\begin{figure}
  \centering
  \includegraphics[width=.7\linewidth]{img/BackProjection.png}
  \caption{Illustration of back projection (source: \url{owlnet.rice.edu/~elec539/Projects97/cult/node2.html})}\label{fig:BP}
\end{figure}

\section{Scatter correction}

The \textit{a priori} scatter correction method originally implemented by
Yang-Kyun Park \cite{Park2015}, as seen in \fref{fig:flowgraph}, takes the
\ac{CB} projections and a \ac{pCT} as input, the \ac{pCT} being the \textit{a
priori} information. The \ac{CB} projections are first loaded into the
software, which (due to a bad choice in compression algorithm by Varian) takes
approximately 30 seconds for 500 projections, in well optimized C++ code. After
that a few filters are applied, most importantly subtraction of a bow-tie-only
projection, if it exists for the data, otherwise (if necessary) a bow-tie like
curve is subtracted. A beam hardening filter can be applied, but is not
necessary as shown by Z{\"o}llner et al \cite{Zollner2017}. The \ac{CB} projections
are then normalized, if there are unrealistic values (like attenuation less
than that of air over the distance from source to detector).\medskip

The resulting projections are then reconstructed with a regular \ac{FDK}
algorithm. A few different combinations of Fourier filters was tested and the
Hann and Cosine Ramp filters was found to generally give the best results.
\footnote{To anyone diving into the code: the reconstruction is followed by
	quite a few geometric filters, to make the result of the
	back-projection be in the same coordinate system as the \ac{pCT}.
	Elekta and Varian does not use the same coordinate systems, and the
\ac{CT} system may not either!}\medskip

The \ac{pCT} is registered to the \ac{rawCBCT}, first manually in our \ac{UI},
then using Plastimatch first rigidly, \ac{rigidCT}, then deformably,
\ac{deformCT}. The Plastimatch registration algorithms has quite a few
implementations, the \ac{OpenMP} and the \ac{CUDA} implementations with the
\ac{MI} and \ac{MSE} as similarity metric was used in this study. Using
\ac{MSE} gave better results in some cases, as observed by Kim et al
\cite{Kim2017}.\medskip

Finally, the scatter correction takes place. The deformed \ac{pCT} is
forward-projected onto the same geometry as the \ac{CB} projections. The
\ac{CB} projection set is then multiplied by a correction factor, which was
first empirically found by Park et al \cite{Park2015}, but we found that the
ratio between the means of the two projection sets gave much better results for
Varian projections\footnote{I suspect the raw Varian CB projections are subject
	to the sensitivity of the sensor-array, which might be the cause of the
very inconsistent results observed with a fixed correction factor, unlike what
has been observed for Elekta projections.}. The residual of the projection sets
are then filtered by a Gaussian and then a median kernel.  However, with the
Varian data artifacts were observed caused by these kernels in areas with large
differences in attenuation. Therefore, these kernels were substituted with a
low-pass filter in Fourier space, which gave very similar results, but fewer
artifacts. The resulting smoothed residual projections represents the simulated
scatter, and these were subtracted from the original projections before finally
reconstructing the now scatter corrected projections with a regular \ac{FDK}
(\ac{corrCBCT}).

%%%%%%%%%%%%%%%
\begin{figure}[ht]
\begin{tikzpicture}[auto, thick, node distance=2cm, >=triangle 45]
\draw
	% Drawing the blocks of first filter :
	node at (20,0)[block, name=input1] {\large $I_{raw}$}
  node [sum, below of=input1] (firstfdk) {\fdk}
  node [block, below of=firstfdk] (rawCBCT) {\large rawCBCT}
  node [ellip, below of=rawCBCT] (regis) {\textbf{Reg.}}
  node [block, node distance = 2cm, left of=regis] (planCT) {\large planCT}
  node [block, text width =5.5em, text centered, node distance = 3cm, right of=regis] (deformCT) {\large rigidCT deformCT}
  node [block, node distance = 3cm, right of=deformCT] (fwdProj) {\large $I_{pri}$}
  node [ellip, above of=fwdProj] (smoothed) {\smooth}
  node [input, node distance = 3cm, left of=smoothed] (help_point) {}
  node [block, above of=smoothed] (scaProj) {\large $I_{sca}$}
  node [sum, above of=scaProj] (sub) {\subt}
  node [block, node distance = 5cm, right of=sub] (corProj) {\large $I_{corr}$}
  node [sum, below of=corProj] (secondfdk) {\fdk}
  node [block, below of=secondfdk] (corrCBCT) {\large corrCBCT};

  \draw[->](input1) -- node {} (firstfdk);
  \draw[->](firstfdk) -- node {} (rawCBCT);
  \draw[->](rawCBCT) -- node {} (regis);
  \draw[->](planCT) -- node {} (regis);
  \draw[->](regis) -- node {} (deformCT);
  \draw[->](deformCT) -- node {} (fwdProj);
  \draw[->](fwdProj) -- node {} (smoothed);
  \draw[-](input1) -- node {} (help_point);
  \draw[->](help_point) -- node {} (smoothed);
  \draw[->](smoothed) -- node {} (scaProj);
  \draw[->](scaProj) -- node {} (sub);
  \draw[->](input1) -- node {} (sub);
  \draw[->](sub) -- node {} (corProj);
  \draw[->](corProj) -- node {} (secondfdk);
  \draw[->](secondfdk) -- node {} (corrCBCT);

\end{tikzpicture}
%%%%%%%%%%%%%%%
\caption{Flow graph of scatter correction algorithm}\label{fig:flowgraph}
\end{figure}


\section{GPU Dose calculation (OpenCL)}\label{sec:gpmc}

Through collaboration with the group who originally created the scatter
correction algorithm, the \ac{goPMC}\cite{Qin2016} was implemented into the CB
reconstruction application. For the purpose of being able to fast and reliably
recalculate the plan on the \ac{corrCBCT}. The \ac{API} of \ac{goPMC} is quite
simple, it takes a \ac{CT} image, a string describing what should be calculated
(dose to water/medium, \ac{LET} or fluence), number of particles, and four
vectors as input. The \ac{CT} must be 512x512 pixels per slice and is rescaled
to 256x256 before the dose calculation. The vectors represent the source
positions, direction, weight and energy of the protons. The application should
be as automated as possible, so using a plan in \ac{DICOM} format the spot
positions are read, and bunch of particle positions is distributed from the
given weight and sigma around each spot, if it is a spot-scanning plan. If it
instead is a passive scatter plan, the compensator thickness is read and the
shape of the collimator, as well as any range modulators or shifters upstream
of the aperture. The energy of the protons is corrected by calculating the
energy loss, using the Bethe formula, over the \ac{WEPL} of the aperture and
range modifiers.  Unlike for the spot scanning, the spot positions are
distributed equally over the "milling tool diameter"\footnote{See
\url{dicom.nema.org/medical/dicom/2014c/output/chtml/part03/sect_C.8.8.25.html}
at (300A, 02E8)}. The weight of each energy given by the width of modulator
wheel levels, were calculated by the relative angle of that modulator level
width to the total rotation angle.\medskip

Three different \ac{DICOM} plans were used for testing, two with \ac{PBS} and
one with passive scatter. Each plan were created in a different \ac{TPS},
Eclipse by Varian and Syngo by Siemens, while the passive scatter plan was from
RayStation by RaySearch Laboratories. The two \ac{PBS} files though
standardized in the \ac{DICOM} format still have differences, most importantly
they use a different direction for the y-axis of the beam coordinate
system.\medskip

\ac{goPMC} was compiled with \ac{MSVC13}, in debug mode.  Debug mode means that
the compiler does not perform any optimizations on the code, which is helpful
when tracking down bugs, but it also makes the application slower. However, as
the application uses \ac{OpenCL} which is just-in-time compiled by the driver,
the slowdown is probably small for \ac{goPMC}, but this also means that when
linking against \ac{goPMC}, we need to also be using \ac{MSVC13} in debug mode.
To avoid having to compile the entire application with that specific compiler
and mode, a small \ac{CLI} wrapper was created around the API, that can be
called from the main application. The \ac{DICOM} plan is read and interpreted
inside the \ac{CLI}, to avoid unnecessarily large file \ac{IO}. However,
without the optimizations from the compiler this meant the code interpreting
the \ac{DICOM} plan and generating the four vectors needed to be optimized
manually, because there are usually tens of thousands of spots in a plan and
the weight, position, direction and energy for all these need to be calculated.
For the position and direction, the data is 3D and thus \ac{SSE2} instructions
could be used to perform the simple arithmetic in parallel in single
instructions. \ac{OpenMP} was used to have the loops running in parallel
utilising as many threads simultaneously as possible. To distribute the
positions, randomly in a Gaussian distribution of a given sigma for each spot,
an inverse \ac{CDF} look-up-table was used, where the random generated number
between $0.0$ and $1.0$ would be compared sequentially against a list of values
rising from $0.0$ to $1.0$ of same length as the \ac{CDF} table, and as soon as
the random value were larger, the same index were used to return the
corresponding value from the \ac{CDF} table. This method is fast when
considering the complexity of actually calculating the integral or using one of
the approximated functions.\medskip

As mentioned, the application was tested on three plans. The spot scanning
plans seem to be calculated correctly, however, the passive scatter plan would
trigger an error on 9 out of 10 runs. Given the randomness of Monte Carlo, this
must be the reason to get different results with running multiple times, with
the same input. This bug remains unsolved, and we will soon integrate the
slightly slower, but more robust and feature-full \ac{gPMC}, as well. Unlike
\ac{goPMC}, I was allowed to compile \ac{gPMC} both in release (meaning full
optimization) and debug mode and with a newer compiler.


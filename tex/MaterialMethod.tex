\chapter{Materials and Methods}

\section{Programming languages, tools, libraries etc.}

For the first study, Python was the language which was primarily used together
with the PyTRiP \ac{API}, along with some \matl~ and R for plotting and
statistics. The second study mainly used C++ with the \ac{API}s from
\ac{DCMTK}, \ac{ITK}, \ac{RTK} and Plastimatch, with some statistics using the
Boost library (\url{boost.org}), plotting of results using R and
Python.\medskip

The compute heavy parts of the application were accelerated using the
\ac{OpenCL} \ac{API} for heterogeneous compute on most hardware. \ac{RTK} and
Plastimatch are optionally accelerated using the programming language for
nVidia \ac{GPU}s, \ac{CUDA}. Thus, the application runs significantly faster
with an nVidia \ac{GPU}. In the future the \ac{CUDA} forward-projection could
be rewritten to use an \ac{OpenCL} implementation. This would potentially make
the application run approximately equally fast on non-nVidia hardware.\medskip

The application \ac{UI} was generated with Qt (\url{qt.io}), a cross-platform
framework and \ac{API} for creating \ac{UI}s. The build system was written in
CMake, an open-source, cross-platform programming language designed to build,
test and package software. Thus, the application can be compiled using any CUDA
compatible compiler for a given operating system \footnote{MacOS may remove
	support for OpenCL in newer versions:
	\url{developer.apple.com/macos/whats-new/}. This OS will therefore not
be supported in the future (OpenCL and GL are definitely not "legacy
technologies", Apple are just trying to force their own API, Metal, onto
developers)}.

\section{Patient cohort and image material}

\subsection{First study}

The study used data sets from eighteen patients with locally advanced prostate
cancer, each set consisting of a \ac{pCT} (Prospeed SX Power, GE Medical
Systems, Milwaukee, WI, USA) and $7-10$ \ac{rCT} scans acquired twice weekly
throughout the course of treatment. The patients were previously treated using
\ac{IMRT} at Haukeland University Hospital, Bergen, Norway. The CT scans were
acquired with the patients in supine position, using the same fixation device
as for treatment.  The images extended from the L4 vertebrae to the inferior
end of the coccyx with a slice thickness of $2-3$ mm and an image size of
$512$x$512$ pixels. All targets and \ac{OR}s were delineated by the same
experienced radiation oncologist for all \ac{pCT}s and \ac{rCT}s. The treatment
protocol and further details have been described previously
\cite{Thornqvist2013b, Thornqvist2011a, Muren2008}. 

\subsection{Second study}

Image data sets of two different phantoms were used, the Alderson phantom (Head
and Thorax section) and the Catphan 504 phantom. Cone beam projections were
acquired on two photon therapy gantries (Varian Clinac \ac{OBI}; TrueBeam
\ac{OBI}; Aarhus University Hospital, Aarhus, Denmark) and at a proton therapy
gantry (Varian ProBeam On-Board Imager; Scripps Proton Therapy Center, San
Diego, CA, USA). The voltage current and pulse length settings of the different
gantries are detailed in \tref{tab:CBsettings} of the supplementary material.  For each
projection data set, a regular \ac{CT} image was acquired with $2-3$mm slice
thickness, image size of $512$x$512$ pixels and extended beyond the limits of
the \ac{CBCT}s in the anterior-posterior directions.

\begin{table}[h]
  \centering
  \begin{tabular}{|l*{4}{c}|}
    \hline
    Phantom:       & \multicolumn{2}{c}{Alderson} & \multicolumn{2}{c|}{Catphan} \\
    \ac{OBI} type: & Clinac        & TrueBeam     & Clinac & ProBeam \\
    \hline
    Voltage [kV]:        & 100-110 & 100          & 100    & 100 \\
    Current [mA]:        & 20      & 20           & 80     & 69  \\
    Pulse length [mSec]: & 20      & 15           & 25     & 15  \\
    \hline
  \end{tabular}
  \caption{Cone Beam \ac{OBI} settings}\label{tab:CBsettings}
\end{table}

\section{WEPL variations}

\subsection{First study}

The average \ac{WEPL} was calculated as the average over each \ac{BEV} ray path
\ac{WEPL} map to the distal surface of the \ac{LN} \ac{CTV} for each beam
angle. The average \ac{WEPL} was calculated for all possible single-beam
configurations, i.e. for both gantry and couch angle in $5^{\circ}$ intervals,
creating a 2D map, with the left and right \ac{LN}s studied separately. The
standard deviation of the absolute differences in the averaged \ac{WEPL}
between the \ac{pCT} and all \ac{rCT}s were subsequently extracted. Further
details of the method can be found in our previous publication
\cite{Casares-Magaz2014b}.  Furthermore, for a couch angle of $0^{\circ}$ the
mean and the standard deviation of the \ac{WEPL} differences, over all
\ac{rCT}s and the \ac{pCT} \ac{WEPL} map, were extracted for every patient in
the population for further analysis.

\subsection{Second study}

In the second study I rewrote the \ac{WEPL} algorithm, from the C extension to 
PyTRiP, to C++ with a simple linear interpolation at each step between the four
nearest pixels. As seen in the supplementary material (Listing \ref{lst:WEPL}),
almost only using the standard library of C++, but also the image class from
\ac{ITK}. This function is quite effecient and can calculate in the order of
$10^5$ WEPL points per second at a depth of $\approx300$mm.\medskip

For all reconstructions and \ac{CT}s, \ac{WEPL} maps were calculated from a
point far anterior to every point posterior of the couch in a grid of 1-by-1 mm
spacing.  The different \ac{WEPL} maps were subtracted from the \ac{WEPL} map
of the \ac{rigidCT} from which mean and standard deviation was extrapolated.
The \ac{rigidCT} were used as the ground truth, because the phantoms used do
not have internal movement.

\section{Treatment planning for beam angle evaluation}

For all gantry angle configurations at couch angle $0^{\circ}$, single beam
spot scanning proton plans were optimized using TRiP \cite{Kramer2004,
Toftegaard2014} avoiding contralateral angles. The plans covered the \ac{LN}
\ac{PTV}, generated from the \ac{CTV} using four different isotropic margin
alternatives ($0, 3, 5$ and $7$ mm), separately for left and right \ac{LN}
volumes. To describe the dose to target and \ac{OR}s with as few variables as
possible, only the target \ac{PTV} for the given side, left or right, was
included in the plan for optimization.  Dose distributions were subsequently
re-calculated on the \ac{rCT}s by applying the optimized fluence maps from the
\ac{pCT} for each beam angle onto all \ac{rCT}s, using the center of mass of
the prostate for rigid translational alignment. For each \ac{CT}, the \ac{DVH}s
and average dose were computed for the right and left \ac{LN} \ac{CTV}, the
bladder and rectum. For six patients (including \ac{pCT}s and 42 \ac{rCT}s in
total) similar calculations were performed also for the bowel cavity.

In the first study the \textit{simple} beam model of TRiP98 were used. Due to
the large amount of data: 37 plans per patient, recalculated $7-10$ times for
each of the 16 patients, with 4 different margins gives $\approx
37\cdot16\cdot8\cdot4 = 18944$ dose calculations to the large lymph node
target.  In the second study, the amount of data were much less and multiple
scatter could be used (taking "only" up to 3 days on an Intel 6800K processor at
$100\%$ usage on all threads, for 72 plans on one CT with four recalculations
with a small target)

\section{Patient clustering comparison}

A k-nearest-neighbour cluster comparison analysis was performed to identify
possible sub-populations of patients with similar patterns in motion robustness
with respect to beam angles \cite{Chaikh2014}.  This analysis compared patients
against angles for $\log_2$ normalised WEPL difference and $\log_2$ normalised
dose degradation. The $\log_2$ normalisation was done by subtracting the mean
of the row from each point in that row, and then dividing by the standard
deviation of that row and finally taking $\log_2$ of that number; the resulting
value was therefore unitless. The patients and angles were grouped and sorted
by similarity using the k-nearest-neighbour distance to identify patient
populations and simultaneously the robust angles for these groups. 

\section{Predictive analysis}

To explore if the results found in this study could be identified using less
data early on in the course of treatment, the analysis as described above was
repeated using only data from the first two rCTs (from the first week) of each
patient. All data analysis was carried out in in-house scripts made for Python
2.7 (www.python.org) \cite{Toftegaard2014}, \matl R2015a (The MathWorks Inc.,
Natick, MA, USA) and the R software (www.r-project.org) \cite{Andersen2015b}.

\section{Xim Projection file format}\label{sec:xim}

The Varian ProBeam \ac{OBI} stores projection images in the same format as the
Varian TrueBeam \ac{OBI}. After obtaining the first test projection sets from a
ProBeam \ac{OBI}, there did not really exist any usable open source reader for
this file format, only a \matl~ script and some documentation
\footnote{\url{bitbucket.org/dmoderesearchtools/ximreader/src}}.  As the
decompression method is the same as for the HND file format, for which there
existed a C++ reader, in \ac{RTK}, this was used as a template. The difference
in the file formats are the meta-data preceding the image data. The meta-data
is completely binary and, when interpreted correctly, contains flags that
describes properties of the image data, such as image dimensions bits per
pixel, whether it is compressed or not, etc..  The decompression can only be
done sequentially one pixel at a time because every pixel depends on the
previous. The \matl~ script takes 30 seconds per projection. Luckily, with the
power bitwise operations in C/C++ this could be brought down to 30 seconds for
500 projections.\medskip


Given an image, $\mathbf{R}$, the compression method can be mathematically written as:
\begin{align}
  \mathbf{R} &= 
    \begin{bmatrix}
      R_{11} & R_{12} & \cdots & R_{1n} \\
      R_{21} & R_{22} & \cdots & R_{2n} \\
      \vdots & \vdots & \ddots & \vdots \\
      R_{m1} & R_{m2} & \cdots & R_{mn}
    \end{bmatrix} \\
  d_{22} &= R_{11} + R_{22} - R_{12} - R_{21} \\
  R_{22} &= d_{22} - R_{11} + R_{12} + R_{21} \\
  R_{i,j} &= d_{i,j} - R_{i-1,j-1} + R_{i-1,j} + R_{i,j-1}
  \label{eq:diff}
\end{align}


The compression works by only storing the difference, $d_{i,j}$, between the
pixels\eqref{eq:diff} (and the first row + one pixel, uncompressed), so if the
difference is small it can be stored in 8 bits, char, larger in 16 bits, short,
and largest in 32 bits, long. However, to determine the bits per difference
value, a look-up-table is needed. This table stores the type of the difference
values indicated by two bits. Therefore, the look-up-table must have a size in
bytes equal to a fourth of the image size. Assuming a projection can be stored
as 2-byte values, which is generally the case, and ignoring the first row for
simplicity: The look-up-table is an eighth the image size. The difference values
must contain as many values as the image, so it is at least half the image size.
In this ideal case the compression of the resulting data would be $5/8$ the size,
or $37.5\%$ less. In the case that half the difference values between
neighbouring pixels were larger than $\pm128$, and had to be stored in 16 bits,
the size would be $7/8$, or $12.5\%$ less. Thus for a noisy image, this
compression would perform poorly. And, as the projections are stored before the
Ramp filter is applied, the projections will usually be noisy. With common
lossless image compression such as zlib or JPEG 2000, approximately the same
compression ratios and possibly better could be achieved, but much faster, due
to the option of multi-threading in these formats. This suggests that either
Varian attempted to impair research which would use their projection data, or
the compression algorithm was written a long time ago and no one has bothered
to look at it since.

\section{Scatter correction method}

The scatter correction method was initially described in \cite{Niu2010} and
recently implemented with a fast user interface using the Qt framework and
modern C++ libraries by Park et al.  \cite{Park2015}. The algorithm takes both
a pCT and the CB projections as input. The projections were initially
reconstructed with RTK \cite{Rit2014} using a regular Feldkamp-Davis-Kress
algorithm, \ac{rawCBCT}, to an image size of 512x512x200 where the pixel size
in the x- and y-direction is determined by the 512 divided by the \ac{FOV}
diameter from the projection geometry and with a slice thickness of 1 mm. The
\ac{pCT} were then registered to the \ac{rawCBCT}, first rigidly, \ac{rigidCT},
where the CT is also interpolated to the same voxel-size as the \ac{rawCBCT},
and then by deformable registration, \ac{deformCT} using Plastimatch
\cite{Shackleford2010} with the \ac{MI} registration method, the \ac{MSE}
method was also tested, but yielded little or no difference for these phantoms.
The \ac{deformCT} were then forward projected onto the same angles as the CB
projections. The raw CB projections were multiplied by a factor determined from
the mean intensity of the two projection sets, before the two projection sets
then were subtracted. These residual projections were then smoothed with a
low-pass Gaussian filter in Fourier space to simulate the scatter, and the beam
hardening\cite{Zollner2017}. The simulated scatter projections were finally
subtracted from the original CB projections before again reconstructing to a
scatter corrected CBCT, \ac{corrCBCT}.

In this project we have modified the application developed by Park et al.
\cite{Park2015} to use projections of the format Varian produces (see section
\ref{sec:xim}), and added the tools necessary to process these, e.g. the
necessary file readers and bow-tie correction filters.  Furthermore, we have
improved the speed and automated the software to calculate optimal options by
itself and thereby rely less on the user, as well as prompt the user for input,
while computations and IO is running, to save time.

\section{Treatment Planning for Dose-guided image quality assessment}

For all gantry angle configurations at couch angle $0^{\circ}$, single beam
spot scanning proton plans were optimized using TRiP98 \cite{Kramer2004,
Toftegaard2014} to a generated sphere of 2.5 cm radius at the centre of the
\ac{rigidCT}. Then, the fluence maps, optimized on the \ac{rigidCT}, for each
beam angle were applied onto all reconstructions. For each reconstruction and
the \ac{rigidCT} the dose was calculated using the "multiple scatter" option in
TRiP98, from which the \ac{DVH}s were computed for the sphere and the entire
dose-cube were kept for gamma analysis. The \ac{DVH}s were normalized to have
$V_{95\%} = 95\%$ for the \ac{rigidCT} and this normalization was applied to
the \ac{DVH}s of the \ac{CB} reconstructions and \ac{deformCT}.

\section{Gamma analysis}

For all the treatment plans the dose distributions were saved and a gamma
analysis\cite{Low1998} was performed comparing the raw-, corr- and
\ac{varianCBCT}s to the \ac{rigidCT}. A threshold dose value was set to $10\%$
of the prescription dose and a dose difference tolerance of 3 and 2 percent of
the prescription dose was used respectively with a spatial tolerance of 3 and
2 mm. The p-value was computed comparing the given CBCT to the \ac{varianCBCT}
using the student’s unequal standard deviation t-test.  All the data analysis
was carried out in in-house scripts made in C++
(\url{gitlab.com/agravgaard/cbctrecon/}), Python 2.7 (\url{www.python.org}) and
R (\url{www.r-project.org}) \cite{Andersen2015b}.

\section{Patient/histogram analysis}\label{sec:hist}

Image data sets of four prostate cancer patients were used, including \ac{pCT}s
with organ delineations and \ac{CBCT} projections from a Varian on-board imager
along with the clinical reconstruction \ac{varianCBCT}.

To evaluate the correctness of the HU values in the \ac{CBCT} reconstructions
we propagated all organ delineations from the \ac{pCT} using rigid
registration. The values inside the structures were then plotted against those
at the same geometrical position in the \ac{deformCT} and histograms were
created of all values in the \ac{CBCT}s and \ac{CT}s.

The correctness was scored as the similarity to the histogram of the
\ac{deformCT} using the Euclidean \ac{EMD}. As we expected the HU distribution
of the bladder and the femur heads to be the least influenced by the
physiological changes between \ac{CT} and \ac{CBCT}, these structures were used
for comparison.


\chapter{Introduction}

\section{Cancer - Radio and Particle Therapy}

Cancer occurs when cells grow abnormally. Often the cell level cause of cancer
is that the cells supply the growth signal themselves, they are insensitive to
the growth stop signal or the apoptotic pathway modified to be strongly
inhibited. These malignant cells proliferate fast and often invade surrounding
tissue fast, this is also why metastasis of cancers are likely to occur,
especially to lymph nodes of the cancerous tumour. Benign tumours do exist, but
are rarely treated with \ac{RT}.  \medskip

To treat a tumour the malignant cells must be killed, this can to some extent
be done by chemotherapy, however as most cancer seems to be patient specific and
because of the fast proliferation this treatment might not be sufficient and
radio therapy or, since the recent growth in the field, particle therapy is
used. \ac{RT} works by sending a beam of high energy photons through the
tumour, the photons interact creating secondary radiation in terms of electrons
which do not travel so far before they ionise nearby molecules leaving radicals
which can damage the DNA and ultimately kill the cells. Therefore it is also
important getting the highest intensity of the beam conformed to the tumour to
kill preferably only the tumour.\medskip

The gain from protons and heavier ions is mainly visible in their Bragg peak
(\fref{fig:bragg}) of dose as a function of depth. These particles also have a
chance of making double strand breaks in the \ac{DNA} of the cells increasing
the biologic effect.  \cite{Kramer2000}

\section{Background}

The established indications for proton therapy, mainly childhood cancer and
selected brain tumours, represent only a minor fraction ($2-3\%$) of the
patients that are treated today with \ac{RT} \cite{Langendijk2013}. However,
given the fundamental characteristics of protons, this modality has a large
potential to improve clinical outcomes for a much larger fraction of cancer
patients \cite{Widesott2011a}, often estimated to $10-15\%$ of all patients
receiving \ac{RT} \cite{Muren2013}.  Most of the tumour sites that represent
potential new proton therapy indications are located in the thorax, abdomen and
pelvis \cite{Langendijk2013}. A common feature of these new indications is that
the precision of treatment is challenged by a considerable degree of intra- and
inter-fractional organ motion, varying across site and also between patients
\cite{Engelsman2013}. With \ac{IMPT} a high and uniform dose across the tumour
volume can be obtained \cite{Lomax1999}, illustrated by the \ac{SOBP} in
\fref{fig:bragg}. However, given the sensitivity of the proton range to density
changes \cite{Engelsman2013}, the dose distribution is at risk of being
degraded unless the uncertainties in range are accounted for \cite{Chen2013,
Park2013, Unkelbach2007, Unkelbach2009}.

\begin{figure}
  \centering
  \includegraphics[width=.7\linewidth]{img/bragg.pdf}
  \caption{Proton stopping power and dose-depth curves based on the Bethe
  equation and the analytical Bragg curve approximation by T. Bortfeld
  \cite{Bortfeld1997}, with a \ac{SOBP}}\label{fig:bragg}
\end{figure}

When irradiating large targets such as the pelvic \ac{LN}s, the influence of
factors such as internal organ motion, weight loss and positioning should be
analysed and considered to ensure that the prescribed dose is delivered
accurately to the target \cite{Thornqvist2013b, Thornqvist2011a}. This is
usually done by adding a margin around the target volumes; however, the
resulting expanded volume could increase to a size where the benefit of
protons, i.e. sparing the normal tissue, is lost. Instead robust treatment plan
optimisation has been reported as being preferable to the use of conventional
margins \cite{Liu2013}. The beam angles giving least density change in the
materials traversed by the proton beams are considered most robust to range
uncertainties.  The range uncertainties are therefore closely coupled to the
changes in the \ac{WEPL} of the proton pencil beams towards the target
\cite{Casares-Magaz2014b, Andersen2015b, Mori2009}. Robust optimization has
been explored by several studies \cite{Chen2013, Liu2013, Liao2016, Li2015,
Liu2012}, however only few studies have so far investigated the advantages of
minimizing the \ac{WEPL} in regard to beam angles for the purpose of
identifying the most robust beam directions for proton therapy
\cite{Casares-Magaz2014b, VanderVoort2016, Chang2014, Mori2008, Matney2016}. In
a previous study from our institution, Casarez-Magaz et al. investigated the
correlation of \ac{WEPL} variation with dose degradation on 4D-\ac{CT} scans of
lung cancer patients \cite{Casares-Magaz2014b}.  Van der Voort et al.
implemented a method for robust optimization by estimating the set-up
uncertainty with lateral beam shifts and the range uncertainty by scaling the
\ac{CT} values \cite{VanderVoort2016}.  We developed a method where
initial results indicated the possibility to identify patient groups with
similar beam angle robustness patterns with respect to inter-fraction motion,
relating \ac{WEPL} changes to degradations in the dose distributions
\cite{Andersen2015b}.\medskip

The potential of proton therapy is currently being explored for a number of
tumour sites influenced by daily variations in anatomy caused e.g. by organ
motion or patient positioning and posture changes.  However, as the proton
range is highly sensitive to density changes \cite{Engelsman2013}, anatomy
changes, organ motion and patient positioning variation could shift and distort
the dose distribution that is actually delivered \cite{Thornqvist2013b,
Thornqvist2011a}. To calculate the impact of such factors, and potentially
adapting for their influence, on-line volumetric imaging throughout the
treatment would be beneficial. The sensitivity of protons also requires a high
quality volumetric image to be calculated reliably. This can be achieved e.g.
by using \ac{CT}-on-rails \cite{Li2013} or MV \ac{CT} \cite{Langen2005}.
Alternatively, the cone-beam imager mounted on the gantry could be used, to
either obtain the anatomy of the patient, on which a \ac{pCT} could be
registered using deformable registration \cite{Elstrom2010}, or by using
software-level improvement of the image quality, to directly calculate the dose
on the improved \ac{CBCT} reconstruction \cite{Park2015, Thing2016}.\medskip

A scatter correction algorithm developed by Niu et al. used the a priori
information of the \ac{pCT} to estimate scatter and remove it from the \ac{CB}
projections before reconstruction \cite{Niu2010, Niu2012}. The algorithm was
subsequently implemented in an open source project \cite{Park2015}, which has
shown great potential for reducing artefacts of \ac{CBCT}s, as shown in a study
investigating inter-fractional \ac{WEPL} differences and dosimetric changes of
proton plan for head and neck patients \cite{Kim2017}.

\subsection{Aim}

The general aim of this study was to show the need of evaluation of a proton
plan throughout a treatment course, i.e. the robustness of said plan, and
provide the means for evaluation through online dose-guidance.\medskip

The aim of the first study was therefore to investigate across a larger
patient population, which beam angles were more robust to inter-fraction
motion in the setting of pelvic \ac{LN} irradiation for prostate cancer
patients. We also aimed to identify any sub-populations of patients with
similar patterns in the motion-robustness across beam angles, as well as
to explore whether the robust angles could be identified using few repeat
\ac{CT} scans for each patient.\medskip

No study has yet investigated the use of the a priori scatter correction method
on \ac{CB} projections from proton gantries.  Therefore, the aim of this study
was to further develop this algorithm and to prove its functionality on \ac{CB}
images from both photon and proton gantries.\medskip

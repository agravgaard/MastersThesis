\chapter{Discussion}

By calculating, analysing and comparing WEPL maps and treatment planning
endpoints for each side of the lymph nodes across the possible beam directions
in a large cohort of patients, we have in this study developed and applied a
method to identify beam angles which were more robust to organ motion for
different sub-groups of patients. Previous studies of beam angle
evaluation/optimisation with respect to plan robustness have explored primarily
intra-fraction motion/uncertainties quantified using 4D-CT scans
\cite{Casares-Magaz2014b, Bernatowicz2013, Bernatowicz2015} or simulations of
set-up uncertainties by isocenter shifts \cite{VanderVoort2016}. Our method
adds to these previous studies as it addresses inter-fraction organ
motion.\medskip

In the first study, we explored the existence of robust angles for
sub-populations of patients with respect to inter-fraction motion, expressed as
variations in WEPL. In our results, we did not observe a direct correlation
between WEPL variation and robustness in terms of dose degradation; however,
the patient populations were separated by having different magnitude of overall
WEPL variation. This could mean that taking the average over each BEV map may
not always be representative for dose degradation in large targets such as the
lymph nodes.\medskip

We identified two main patient sub-populations using the WEPL variation, with
each group having a preference in terms of \ac{OR} dose: In one group the more
posterior angles were preferable and in one group the lateral towards anterior
angles. These groups did not show any preference in terms of target dose. This
suggests that the variation in WEPL may be a good metric for predicting which
sub-populations patients belong to.\medskip

The results were left-right symmetric for most organs. However, for the rectum
we found a tendency of higher robustness of low mean dose around $165^{\circ}$
for the right side as compared to the anterior angles, whereas the opposite was
seen for the bladder \fref{fig:polar_mean}.  When observing the dose maps in
the re-calculated plans the primary dose degradation were at the distal end of
the beam either overshooting or undershooting the target. Overshooting leads to
an increased dose behind the target in the beam direction, therefore the more
posterior angles were found to be more robust for the rectum. Chronic rectal
bleeding and loss of bowel function are relatively common late side effects
\cite{Michalski2010}, for this reason the posterior angles may be recommended
for being most robust, with best sparing for the bowel and rectum. For the
bladder, however, the bladder filling may lead to the bladder entering the
field laterally to the beam direction in the anterior to lateral beam angles,
and two primary patient populations with different angle robustness were
observed. Previous studies have shown “hot-spots” in the bladder to be
associated with late genitourinary toxicity \cite{Cheung2007}, and should
therefore be avoided. For this reason, and because the rectum does not have a
general robust angle from the left side for all the patients, robust angles
specific for sub-populations of patients appear beneficial and should be
further explored.\medskip

The plans in the fisrt study were created using isotropic \ac{CTV}-to-\ac{PTV}
margin expansion, and with no consideration of \ac{OR}s in the dose
optimisation.  Different optimisation algorithms and priorities in the
optimisation would influence the results. Our results showed that $3$ mm
isotropic margin might be too small for some cases, while $5$ mm might give
sufficient target coverage throughout the entire treatment course.  Using
margins of $7$ mm showed no significant difference from $5$ mm, except
increasing the \ac{OR} dose. It should be pointed out that this study did not
consider setup-errors (only relative motion between the prostate and the
\ac{LN}s) as the \ac{rCT}s were aligned to the center of the prostate, however
modern treatment planning systems have solutions developed to take such
uncertainties into account using isocenter shifts during the treatment planning
optimization \cite{VanderVoort2016, Fredriksson2011, Eclipse13}.\medskip

When the mean dose analysis was performed using only the first two \ac{rCT}s
the same pattern as in the full analysis was observed.  The magnitude of the
dose degradation described using only two \ac{CT}s was depending on the organ,
with rectum and bladder showing larger deviations as compared to the use of all
\ac{rCT}s. This shows that the patient group and angles of least dose
degradation may be predictable using fewer scans, opening the potential for
adaptive strategies.  This however, requires high quality on-line imaging, to
be able to reliably (re-)evaluate the plan, e.g. using cone beam \ac{CT}
(\ac{CBCT}) images. Recent studies have developed methods that use the
\ac{CBCT} image along with the \ac{pCT} for fast plan re-evaluation and
potentially adaptation \cite{Park2015, Veiga2016, Thing2016, Kurz2015} e.g.
using fast online re-optimisation \cite{Unkelbach2016}.\medskip

This study only used the prostatic \ac{LN}s as target for simplicity and
therefore the angles found to be more robust does not necessarily apply for the
prostate itself \cite{Cao2015}. In ongoing work, we are exploring whether the
same patterns of robustness are found when plans treating both the primary
(prostate) and elective targets (including \ac{LN}s) are created in a clinical
(and commercial) treatment planning system. In future work, we will explore how
this method could be clinically implemented in combination with the \ac{CBCT}
scatter correction.  Furthermore, this study explored the pelvic lymph nodes as
target, however the method could be applied to any target and the appropriate
\ac{OR}s.\bigskip

%%%%%%%%%%%% End first study %%%%%%%%%%

Recent studies have developed methods for adapting the cone-beam image of the
day to the \ac{pCT} for re-evaluation of the plan on-line and adapt if
necessary \cite{Park2015, Thing2016, Veiga2016, Kurz2015}. In the second study,
we have adapted the method, implemented by Y-K. Park et al. \cite{Park2015}, to
be usable across platforms \cite{gitlabrepo} for potential implementation in a
clinical work-flow. The method introduced initially by T.  Niu et al.
\cite{Niu2010} compared the results to those of the asymmetric scatter kernel
superposition method (ASKS), proposed by J. Star-Lack et al.
\cite{Star-Lack2009}, and currently used in Varian TrueBeam systems. They found
that compared to the ASKS this method reduces image distortions near the object
centre and results in greater overall uniformity.  However, if there are
“errors” in the deformed \ac{pCT} these might create blooming artefacts in the
corrected \ac{CBCT}, but as the ASKS method does not require prior information
it will not create these artefacts. Y-K. Park et al. compared the method to the
uniform scatter correction method which only showed small improvements over the
raw\ac{CBCT} compared to those of the a priori scatter correction.\medskip

A study by C. Veiga et al. presents some of the first clinical results of an
on-line adaptation workflow for proton therapy \cite{Veiga2016}. Their method
uses rigid and deform registration of the \ac{pCT} of lung cancer patients to the
\ac{CBCT} followed by manual modification to accommodate the changes in tumour size.
The dose deterioration of the proton plan was measured by water equivalent
thickness and dose volume histograms.  They concluded that the method could
provide “clinical indicators” similar to those of a repeat \ac{CT} scan. However,
they also note that there are room for improvement in the workflow for example
the validation of segmentation of volumes of interest could be done
automatically \cite{Peroni2012}, and the dose re-calculation speed and accuracy
could be improved \cite{Jia2012}.\medskip

A study by J. Kim et al. \cite{Kim2017} has used the same implementation of the
a priori scatter correction \cite{Park2015} to improve the quality of weekly
\ac{CBCT}s for $13$ of head and neck patients. This allowed them to compare the
WEPL differences to the distal surface of delineated tumours throughout
treatment as a measure of the proton dose deterioration with a range
calculation uncertainty of $2\%$.  Additionally, they found that the \ac{MI}
registration method would sometimes have problems distinguishing the
immobilization mask from the skin, while the \ac{MSE} method performed better
in this regard and might therefore be preferable.\medskip

We have found that the a priori scatter correction can improve the \ac{CB}
reconstruction significantly from the clinical standard in most our test cases
(non-significant improvement in the last case), and a larger improvement was
observed in regions of sharp density changes, such as air-to-bone (angles
around $180^{\circ}$ in \fref{fig:alderson_dose}). This suggests that the
correction will be most beneficial in the regions were protons are more
sensitive to changes \cite{Placidi2016}. The analysis of the Catphan phantom
showed that the improvement was not exclusive to the older Clinac \ac{CB}, but
also applicable to modern ProBeam \ac{CB}. The ProBeam \ac{CB}, however, used a
different pulse current and a direct comparison of the quality of these \ac{CB}
configurations should therefore be avoided.\medskip

This study only used phantom data, which means that new artefacts and problems
could arise when using the algorithm on patient data, however, we have already
implemented tools like those used in other clinical studies \cite{Veiga2016} to
take care of potential issues, e.g.  movement of air or extreme tumour
growth/shrinkage.  Furthermore, the same method has been applied on patient
data from Elekta systems in other studies \cite{Kim2017, Kurz2016, Kurz2016a}
and has shown impressive results.\medskip

A complete a priori scatter correction can be performed in less than four
minutes on a modern laptop computer with an nVidia graphics card. The majority
of the time would be spent on user input, during the initial manual
registration. The amount of manual intervention required can be reduced if
sufficient information from \ac{CB} and \ac{CT} scanner is standardised and
known at compile-time. Using the deformation of the \ac{pCT} to the
raw\ac{CBCT} on the contours of the \ac{pCT} \cite{Thornqvist2013,
Thornqvist2010, Thor2011}, as well as a fast reliable proton dose calculation
engine \cite{Jia2012, Tian2015, Qin2016}, it would be possible to recalculate
the plan in minutes, opening the potential for adaptive strategies.
